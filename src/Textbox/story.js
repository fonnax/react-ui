import React from 'react'
import { storiesOf } from '@storybook/react'
import { TextBox } from './'

storiesOf('TextBox', module)
  .add('Normal', () => (
    <TextBox />
  ))
  .add('With Placeholder', () => (
    <TextBox placeholder="placeholder" />
  ))
