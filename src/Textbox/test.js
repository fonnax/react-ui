import { render, cleanup } from 'react-testing-library'

import React from 'react'
import { TextBox } from './'

afterEach(cleanup)

it('A TextBox is rendered', () => {
  const { queryByPlaceholderText, getByDisplayValue } = render(
    <TextBox />
  )

  expect(getByDisplayValue('')).toBeTruthy()
  expect(queryByPlaceholderText(/placeholder/i)).toBeNull()
})

it('A TextBox is rendered with a placeholder', () => {
  const { getByPlaceholderText, queryByPlaceholderText } = render(
    <TextBox placeholder="placeholder" />
  )

  expect(getByPlaceholderText(/placeholder/i)).toBeTruthy()
  expect(queryByPlaceholderText('')).toBeNull()
})
