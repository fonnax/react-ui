import React from 'react'

export const TextBox = ({ placeholder }) => (
  <input type="text" placeholder={ placeholder } />
)
